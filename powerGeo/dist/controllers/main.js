"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const USState_1 = __importDefault(require("../models/USState"));
const Powerplant_1 = __importDefault(require("../models/Powerplant"));
const mongoose_1 = __importDefault(require("mongoose"));
/**
 * @GET ('api/getStates')
 * @Return a list of states from the database
 */
const getStates = function (req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const states = yield USState_1.default.find().exec();
            return res.status(200).json(states);
        }
        catch (error) {
            return res.status(400).json({
                message: error._message,
                error: error
            });
        }
    });
};
/**
 * @GET ('api/getPowerPlants')
 * @params state (Optional)
 * @Return complete or state based power plants list
 */
const getPowerPlants = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { state } = req.params;
        const powerPlants = state ? yield Powerplant_1.default.find({ state_symbol: state.toUpperCase() }).exec() : yield Powerplant_1.default.find().exec();
        return res.status(200).json(powerPlants);
    }
    catch (error) {
        return res.status(400).json({
            message: error._message,
            error: error
        });
    }
});
/**
 * @GET ('api/getTopPowerPlants')
 * @Return top 5 power plants list based on net generation
 */
const getTopPowerPlants = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const powerPlants = yield Powerplant_1.default.find().sort({ net_generation: -1 }).limit(5).exec();
        return res.status(200).json(powerPlants);
    }
    catch (error) {
        return res.status(400).json({
            message: error._message,
            error: error
        });
    }
});
/**
 * @POST ('api/addPowerPlants')
 * @params (Mandatory Fields) name, state_symbol, Lat, Long, net_generation, net_percentage
 * @Description Add Power Plant into database
 * @Return added power plant
 */
const addPowerPlants = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { name, state_symbol, Lat, Long, net_generation, net_percentage } = req.body;
        const powerplant = new Powerplant_1.default({
            _id: new mongoose_1.default.Types.ObjectId(),
            name,
            state_symbol,
            Lat,
            Long,
            net_generation,
            net_percentage
        });
        const result = yield powerplant.save();
        return res.status(200).json(result);
    }
    catch (error) {
        return res.status(400).json({
            message: error._message,
            error: error.errors
        });
    }
});
exports.default = { getStates, getPowerPlants, getTopPowerPlants, addPowerPlants };
//# sourceMappingURL=main.js.map