"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CErrors = void 0;
/** Custom enum for application errors */
var CErrors;
(function (CErrors) {
    CErrors["PARAMS_MISSING"] = "Params missing";
    CErrors["INVALID_REQUEST"] = "Invalid Request";
})(CErrors = exports.CErrors || (exports.CErrors = {}));
//# sourceMappingURL=CErrors.js.map