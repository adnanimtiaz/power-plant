"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const mongoose_2 = require("mongoose");
const PowerPlantSchema = new mongoose_2.Schema({
    name: { type: String, required: true },
    state_symbol: { type: String, required: true },
    Lat: { type: String, required: true },
    Long: { type: String, required: true },
    net_generation: { type: Number, required: true },
    net_percentage: { type: Number, required: true },
    state: {
        type: mongoose_2.Schema.Types.ObjectId, ref: 'states'
    }
}, {
    timestamps: true
});
exports.default = mongoose_1.default.model('powerplants', PowerPlantSchema);
//# sourceMappingURL=Powerplant.js.map