"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
const express_1 = __importDefault(require("express"));
const main_1 = __importDefault(require("../controllers/main"));
const router = express_1.default.Router();
/**
 * Complete application routing entry points
 */
router.get('/getStates', main_1.default.getStates);
router.get('/powerplants/:state?', main_1.default.getPowerPlants);
router.get('/topPowerplants', main_1.default.getTopPowerPlants);
router.post('/powerplants', main_1.default.addPowerPlants);
module.exports = router;
//# sourceMappingURL=api.js.map