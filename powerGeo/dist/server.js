"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const path = __importStar(require("path"));
const http = __importStar(require("http"));
const api_1 = __importDefault(require("./routes/api"));
const config_1 = __importDefault(require("./config/config"));
const logging_1 = __importDefault(require("./config/logging"));
const mongoose_1 = __importDefault(require("mongoose"));
const GMessages_1 = require("./enums/GMessages");
const swaggerUi = require("swagger-ui-express");
const fs = require("fs");
/**
 * Setup of express application, loading configurations
 */
class Server {
    constructor() {
        this.TAG = 'Server';
        /* Swagger files start */
        this.swaggerFile = (process.cwd() + "/swagger.json");
        this.swaggerData = fs.readFileSync(this.swaggerFile, 'utf8');
        this.swaggerDocument = JSON.parse(this.swaggerData);
        this.app = (0, express_1.default)();
        this.loadConfigurations();
    }
    /* Swagger files end */
    static bootstrap() {
        return new Server();
    }
    getServer() {
        return this.app;
    }
    /** Loading server configurations & server instance */
    loadConfigurations() {
        return __awaiter(this, void 0, void 0, function* () {
            /** Connect to Mongo */
            mongoose_1.default
                .connect(config_1.default.mongo.url, config_1.default.mongo.options)
                .then((_result) => {
                logging_1.default.info(this.TAG, GMessages_1.GMessages.DATABASE_CONNECTED);
            })
                .catch((error) => {
                logging_1.default.error(this.TAG, error.message, error);
            });
            this.app.use(express_1.default.json());
            this.app.use(express_1.default.urlencoded({
                extended: true
            }));
            this.app.use(express_1.default.static(path.join(__dirname, 'public')));
            this.app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(this.swaggerDocument, null, null, null));
            const server = http.createServer(this.app);
            server.listen(config_1.default.server.port, () => console.log(`API running on ${config_1.default.server.hostname}:${config_1.default.server.port}`));
            this.loadRoutes();
        });
    }
    /** Loading all server routes */
    loadRoutes() {
        /** Log the request */
        this.app.use((req, res, next) => {
            /** Log the req */
            logging_1.default.info(this.TAG, `METHOD: [${req.method}] - URL: [${req.url}] - IP: [${req.socket.remoteAddress}]`);
            res.on('finish', () => {
                /** Log the res */
                logging_1.default.info(this.TAG, `METHOD: [${req.method}] - URL: [${req.url}] - STATUS: [${res.statusCode}] - IP: [${req.socket.remoteAddress}]`);
            });
            next();
        });
        this.app.use((req, res, next) => {
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
            if (req.method == 'OPTIONS') {
                res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
                return res.status(200).json({});
            }
            next();
        });
        this.app.use('/api', api_1.default);
    }
}
exports.default = Server;
//# sourceMappingURL=server.js.map