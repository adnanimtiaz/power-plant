import { Request, Response } from 'express';
import USState from '../models/USState';
import PowerPlant from '../models/Powerplant';
import mongoose from 'mongoose';

/**
 * @GET ('api/getStates')
 * @Return a list of states from the database
 */
const getStates = async function(req: Request, res: Response) {
    try {
        const states = await USState.find().exec();
        return res.status(200).json(states);
    } catch (error) {
        return res.status(400).json({
            message: error._message,
            error: error
        });
    }
};

/**
 * @GET ('api/getPowerPlants')
 * @params state (Optional)
 * @Return complete or state based power plants list
 */
const getPowerPlants = async (req: Request, res: Response) => {
    try {
        const { state } = req.params; 

        const powerPlants = state? await PowerPlant.find({state_symbol: state.toUpperCase()}).exec() : await PowerPlant.find().exec();
        return res.status(200).json(powerPlants);
    } catch (error) {
        return res.status(400).json({
            message: error._message,
            error: error
        });
    }
};

/**
 * @GET ('api/getTopPowerPlants')
 * @Return top 5 power plants list based on net generation
 */
const getTopPowerPlants = async (req: Request, res: Response) => {
    try {
        const powerPlants = await PowerPlant.find().sort({net_generation: -1}).limit(5).exec();
        return res.status(200).json(powerPlants);
    } catch (error) {
        return res.status(400).json({
            message: error._message,
            error: error
        });
    }
};

/**
 * @POST ('api/addPowerPlants')
 * @params (Mandatory Fields) name, state_symbol, Lat, Long, net_generation, net_percentage
 * @Description Add Power Plant into database
 * @Return added power plant
 */
const addPowerPlants = async (req: Request, res: Response) => {
    try {
        const { name, state_symbol, Lat, Long, net_generation, net_percentage } = req.body;

        const powerplant = new PowerPlant({
            _id: new mongoose.Types.ObjectId(),
            name,
            state_symbol,
            Lat,
            Long,
            net_generation,
            net_percentage
        });

        const result = await powerplant.save();

        return res.status(200).json(result);
    } catch (error) {
        return res.status(400).json({
            message: error._message,
            error: error.errors
        });
    }
};

export default { getStates, getPowerPlants, getTopPowerPlants, addPowerPlants };
