/** Custom enum for application errors */
export enum CErrors {
    PARAMS_MISSING = "Params missing",
    INVALID_REQUEST = "Invalid Request",
}