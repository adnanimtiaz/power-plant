import { Document } from 'mongoose';

export default interface PowerPlant extends Document {
    name : string;
    stateSymbol: string;
    netGeneration: number;
    netPercentage: number;
    Lat: string;
    Long: string;
}