import { Document } from 'mongoose';

export default interface USState extends Document {
    title : string;
    symbol: string;
}