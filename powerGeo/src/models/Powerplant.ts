import mongoose from 'mongoose';
import { Schema } from 'mongoose';
import PowerPlant from '../interfaces/powerplant';

const PowerPlantSchema: Schema = new Schema ({
    name: { type: String, required: true },
    state_symbol: { type: String, required: true },
    Lat: { type: String, required: true },
    Long: { type: String, required: true },
    net_generation: { type: Number, required: true },
    net_percentage: { type: Number, required: true },
    state: {
        type: Schema.Types.ObjectId, ref: 'states'
    }
}, {
    timestamps: true
});

export default mongoose.model<PowerPlant>('powerplants', PowerPlantSchema);