import mongoose from 'mongoose';
import { Schema } from 'mongoose';
import USState from '../interfaces/USState';

const StateSchema: Schema = new Schema ({
    title: { type: String, required: true },
    symbol: { type: String, required: true }
}, {
    timestamps: true
})

export default mongoose.model<USState>('states', StateSchema);