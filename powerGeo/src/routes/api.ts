import express from 'express'; 
import controller from '../controllers/main';

const router = express.Router();
/**
 * Complete application routing entry points
 */
router.get('/getStates', controller.getStates);
router.get('/powerplants/:state?', controller.getPowerPlants);
router.get('/topPowerplants', controller.getTopPowerPlants);
router.post('/powerplants', controller.addPowerPlants);

export = router;