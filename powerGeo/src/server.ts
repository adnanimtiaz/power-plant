import express from "express";
import * as path from 'path'; 
import * as http from 'http'; 
import api from './routes/api';
import config from './config/config';
import logging from './config/logging';
import mongoose from 'mongoose';
import { GMessages } from './enums/GMessages';
import swaggerUi = require('swagger-ui-express');
import fs = require('fs');

/**
 * Setup of express application, loading configurations
 */
export default class Server { 
    public app;
    TAG = 'Server';

      /* Swagger files start */
      private swaggerFile: any = (process.cwd()+"/swagger.json");
      private swaggerData: any = fs.readFileSync(this.swaggerFile, 'utf8');
      private swaggerDocument = JSON.parse(this.swaggerData);
      /* Swagger files end */

    public static bootstrap(): Server { 
        return new Server(); 
    } 

    public getServer() { 
        return this.app;
    } 
  
    constructor() { 
        this.app = express(); 
        this.loadConfigurations();
    } 

    /** Loading server configurations & server instance */
    public async loadConfigurations() { 
        /** Connect to Mongo */
        mongoose
        .connect(config.mongo.url, config.mongo.options)
        .then((_result) => {
            logging.info(this.TAG, GMessages.DATABASE_CONNECTED);
        })
        .catch((error) => {
            logging.error(this.TAG, error.message, error);
        });


        this.app.use(express.json());
        this.app.use(express.urlencoded({
            extended: true
        }));
      
        this.app.use(express.static(path.join(__dirname, 'public'))); 
        this.app.use('/api-docs', swaggerUi.serve,
            swaggerUi.setup(this.swaggerDocument, null, null, null));
       
        const server = http.createServer(this.app); 
      
        server.listen(config.server.port, () => console.log(`API running on ${config.server.hostname}:${config.server.port}`));

        this.loadRoutes();
    }

    /** Loading all server routes */
    private loadRoutes() { 
        /** Log the request */
        this.app.use((req, res, next) => {
            /** Log the req */
            logging.info(this.TAG, `METHOD: [${req.method}] - URL: [${req.url}] - IP: [${req.socket.remoteAddress}]`);

            res.on('finish', () => {
                /** Log the res */
                logging.info(this.TAG, `METHOD: [${req.method}] - URL: [${req.url}] - STATUS: [${res.statusCode}] - IP: [${req.socket.remoteAddress}]`);
            })
            
            next();
        });

        this.app.use((req, res, next) => {
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
        
            if (req.method == 'OPTIONS') {
                res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
                return res.status(200).json({});
            }
        
            next();
        });

        this.app.use('/api', api);
    }
}