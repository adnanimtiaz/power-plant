import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';
import { USStates } from './models/USStates.model';
import { PowerPlant } from './models/PowerPlant.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'powerGeoFace';

  statesList?: USStates[];
  powerPlants?: PowerPlant[];

  lat = 41.356196;
  lng = -108.463487;

  zoom: number = 4;
  topSearch: boolean = true; 

  constructor(private appService: AppService) { 
    this.appService.getStates().subscribe({next: (res) => {
      this.statesList = res;
    }})

    this.loadTopPowerPlants();
  }

  parse(val: string): number {
    return parseFloat(val);
  }

  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`)
  }

  changeState(e: any) {
    this.appService.getPowerPlants(e.target.value).subscribe({next: (res) => {
      this.powerPlants = res;
    }})
    console.log(e.target.value);
  }

  changeSidePanel(val: string) {
    switch(val){
      case 'TOP':
        this.topSearch = true;
        this.loadTopPowerPlants();
        break;

      case 'SEARCH':
        this.topSearch = false;
        this.loadAllPowerPlants();
        break;
    }
  }

  /**
   * Loading Top Power plants from API call
   */
  loadTopPowerPlants(){
    this.appService.getTopPowerPlants().subscribe({next: (res) => {
      this.powerPlants = res;
    }});
  }

  /**
   * Loading All or search based Power plants from API call
   */
  loadAllPowerPlants(){
    this.appService.getPowerPlants().subscribe({next: (res) => {
      this.powerPlants = res;
    }});
  }
}
