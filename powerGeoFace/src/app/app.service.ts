import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from './../environments/environment';
import { USStates } from './models/USStates.model';
import { PowerPlant } from './models/PowerPlant.model';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  API_URL: any;

  constructor(private http: HttpClient) { 
    this.API_URL = environment.API;
  }

  getStates(): Observable<USStates[]> {
    return this.http.get<USStates[]>(this.API_URL.GET_STATES);
  }

  getPowerPlants(state?: string): Observable<PowerPlant[]> {
    return this.http.get<PowerPlant[]>(state? this.API_URL.GET_POWER_PLANTS + `/${state}` : this.API_URL.GET_POWER_PLANTS);
  }

  getTopPowerPlants(state?: string): Observable<PowerPlant[]> {
    return this.http.get<PowerPlant[]>(this.API_URL.GET_TOP_POWER_PLANTS);
  }

}
