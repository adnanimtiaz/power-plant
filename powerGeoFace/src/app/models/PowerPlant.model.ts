export class PowerPlant {
    name!: string;
    state_symbol!: string;
    net_generation!: number;
    net_percentage!: number;
    Lat!: string;
    Long!: string;
}